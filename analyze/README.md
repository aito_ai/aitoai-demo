## Analyze Products Data

Aito can analyze the products data to predict the result and explain the prediction

## The data
1. The training dataset is a dataset that contains information about products
2. The training dataset is taken from [Kaggle public dataset](https://www.kaggle.com/PromptCloudHQ/all-jc-penny-products)
3. This is a pre-crawled dataset, taken as a subset of a bigger dataset (more than 3.7 million products) that was created by extracting data from jcpenney.com, a well known retailer.
4. The dataset was preprocessed to remove and fix bad data. For example: wrong column, missing data, etc.
5. The dataset contains 10148 rows for 10148 products with the following columns (features of each product):

* sku
* name_title
* description
* list_price
* sale_price
* category
* category_tree
* average_product_rating
* product_url
* product_image_urls
* brand
* total_number_reviews
* reviews


## Example usage

### 1. Based on a product title, predict category

* __Using API__: __*Predict*__
* __Send query to https://aitoai-demo.api.aito.ninja/api/_predict__
* __Example:__

From the product name "Levi’s® 541™ Athletic Fit Jeans - Boys 8-20"

[Query](queries/title_levi_to_category.json)

	{
	   "context": "products",
	    "q" : [
	    	{"when":"name_title", "is":"Levi’s® 541™ Athletic Fit Jeans - Boys 8-20"}
	   ],
	   "predict":"category"
	}

This query commands aito to predict the _category_ of the product given that this product has _name_title_ _"Levi’s® 541™ Athletic Fit Jeans - Boys 8-20"_

[Expected Result](results/title_levi_to_category.json)

This is what aito returns. Let's interpret the result: 

	[
    {
        "probability": 0.09123927941104769,
        "variable": {
            "feature": "boys husky",
            "field": "category",
            "id": "category:boys husky",
            "priorW": 2,
            "priorP": 0.5
        }
    },
    {
        "probability": 0.0828966888971313,
        "variable": {
            "feature": "levi's",
            "field": "category",
            "id": "category:levi's",
            "priorW": 2,
            "priorP": 0.5
        }
    },
    {
        "probability": 0.07695965917702025,
        "variable": {
            "feature": "jeans",
            "field": "category",
            "id": "category:jeans",
            "priorW": 2,
            "priorP": 0.5
        }
    },
    {
        "probability": 0.07485490274469501,
        "variable": {
            "feature": "big kid (6-20)",
            "field": "category",
            "id": "category:big kid (6-20)",
            "priorW": 2,
            "priorP": 0.5
        }
    },
    ...

Aito.ai predicts that with the given title _"Levi’s® 541™ Athletic Fit Jeans - Boys 8-20"_ and based on the training dataset, the most likely category of the product is _"boys husky"_, the second most likely category is _"levi"_, following _"jeans"_ and _"big kid"_. 
This is interesting since the actual category in the training data is _"Levi"_. Hence, we can say that Aito.ai predicts incorrectly. However, Aito.ai is able to make the human-like prediction that fits the context. _"boys husky"_, _"jeans"_, and _"big kid (6-20)"_ can all be the category for this product. This example shows that Aito.ai does not only aim for the prediction accuracy like most machine learning model, it aims to produce results that fit the context.
Moreover, the core problem of machine learning is to be able to predict the future data, not to predict correctly the training data and Aito.ai can do this extremely well.
It is also worth to mention that the query given to Aito was really simple and comprehensive. 

* __Lets try another query__
From the title "adidas® 3G Speed Shorts-Big & Tall"

* __Using API__: __*Predict*__
* __Send query to https://aitoai-demo.api.aito.ninja/api/_predict__

[Query](queries/title_adidas_to_category.json)

	{
	   "context": "products",
	    "q" : [
	    	{"when":"name_title", "is":"adidas® 3G Speed Shorts-Big & Tall"}
	   ],
	   "predict":"category"
	}


[Expected Result](results/title_adidas_to_category.json)

	[
	    {
	        "probability": 0.11330235643248021,
	        "variable": {
	            "feature": "athletic",
	            "field": "category",
	            "id": "category:athletic",
	            "priorW": 2,
	            "priorP": 0.5
	        }
	    },
	    {
	        "probability": 0.10968761404996058,
	        "variable": {
	            "feature": "shorts",
	            "field": "category",
	            "id": "category:shorts",
	            "priorW": 2,
	            "priorP": 0.5
	        }
	    },
	    {
	        "probability": 0.08615545618122993,
	        "variable": {
	            "feature": "kids athletic shoes",
	            "field": "category",
	            "id": "category:kids athletic shoes",
	            "priorW": 2,
	            "priorP": 0.5
	        }
	    },
	    {
	        "probability": 0.05191579012859051,
	        "variable": {
	            "feature": "workout clothes",
	            "field": "category",
	            "id": "category:workout clothes",
	            "priorW": 2,
	            "priorP": 0.5
	        }
	    },
	    ...

Again, Aito.ai is not able to correctly predict the category according to the training data since the actual category in the training dataset is _"shorts"_. 
However, aito returns good results that fit the context: _"athletic"_, _"kids athletic shoes"_, _"workout clothes"_

### 2. Examine aito.ai's logic

If we are skeptical of the result, or simply want to know the "thinking" behind aito's prediction, we can look at aito's explanation by:

* __Using API__: __*Relate*__
* __Send query to https://aitoai-demo.api.aito.ninja/api/_relate__
* __Example:__

[Query](queries/relate_title_adidas_to_category_atheletic.json)

		{
			"context": "products",
			"q" : [
				{"when":"name_title", "is": "adidas® 3G Speed Shorts-Big & Tall"}
			],
			"to":[{"field":"category", "is":"athletic"}]
		}

This query command aito to show the stastistical relationship between a product with _name_title_ _"adidas® 3G Speed Shorts-Big & Tall"_ and _category_ is _"athletic"_. This helps us to understand why aito would predict _"athletic"_ instead of _"shorts"_

[Expected Result](results/relate_title_adidas_to_category_atheletic.json)

aito.ai returns a lot of stastical number but let's take a look at a small part of it for example

	{
                "feature": "athletic",
                "field": "category",
                "conditional": [
                    ...
                    {
                        "negationLift": 0.9888825449516625,
                        "samples": 89,
                        "whenVariableStates": [
                            {
                                "index": 0,
                                "state": true,
                                "field": "name_title",
                                "feature": "adida"
                            }
                        ],
                        "frequency": 2,
                        "lift": 57.36916674741727,
                        "probability": 0.01131244975964649
                    }
                ],
                "frequency": 2,
                "index": 1,
                "probability": 0.00019718692811859206,
                "variableId": "category:athletic"

Aito explains that it estimates the probability of _category_ is _"athletic"_ when _name_title_ has _"adidas"_ is 0.0113, much higher than the average probability of _category_ is _"athletic"_ which is 0.000197. More specifically, when _name_title_ has _"adidas"_ in it, it is 57 times more likely (_lift_) that the _category_ would be _"athletic"_. This smart estimation of aito is based on upgraded Naive Bayes. 

### 3. Based on product description, predict category

Similarly, aito.ai can predict the category based on the product description

* __Using API__: __*Predict*__
* __Send query to https://aitoai-demo.api.aito.ninja/api/_predict__

[Query](queries/desc_levi_to_category.json)

	{
	   "context": "products",
	    "q" : [
	    	{"when":"description", "is":"Levi's 505 regular-fit jeans are made for the guy who wants comfort with great style.     matching big & tall web id: 5834009 sits at waist regular, comfortable fit through seat and thigh straight legs, 16½ \" opening skunk train and Big root colors: 98% cotton/2% elastane cash: 99.13% cotton/.87% elastane all other colors: 100% cotton green leaf: cotton/polyester/elastane washable imported more sizes: regular, extra tall, big"}
	   ],
	   "predict":"category"
	}

[Expected Result](results/desc_levi_to_category.json)

	[
	    {
	        "probability": 0.007497876438449509,
	        "variable": {
	            "feature": "levi's",
	            "field": "category",
	            "id": "category:levi's",
	            "priorW": 2,
	            "priorP": 0.5
	        }
	    },
	    {
	        "probability": 0.007497876435281275,
	        "variable": {
	            "feature": "levi’s 559™ relaxed straight",
	            "field": "category",
	            "id": "category:levi’s 559™ relaxed straight",
	            "priorW": 2,
	            "priorP": 0.5
	        }
	    },
	    ...

In this case, aito predicts correctly that the _category_ is _"levi's"_, but also providest the good second result which is _"levi’s 559™ relaxed straight"_.

This means that we can improve aito prediction by adding information to the query

* __Using API__: __*Predict*__
* __Send query to https://aitoai-demo.api.aito.ninja/api/_predict__

[Query](queries/desc_title_levi_to_category.json)

	{
	   "context": "products",
	    "q" : [
	    	{"when":"name_title", "is":"Levi’s® 541™ Athletic Fit Jeans - Boys 8-20"},
	    	{"when":"description", "is":"Levi's 505 regular-fit jeans are made for the guy who wants comfort with great style.     matching big & tall web id: 5834009 sits at waist regular, comfortable fit through seat and thigh straight legs, 16½ \" opening skunk train and Big root colors: 98% cotton/2% elastane cash: 99.13% cotton/.87% elastane all other colors: 100% cotton green leaf: cotton/polyester/elastane washable imported more sizes: regular, extra tall, big"}
	   ],
	   "predict":"category"
	}

[Expected Result](results/desc_title_levi_to_category.json)

	{
	        "probability": 0.009240542571666082,
	        "variable": {
	            "feature": "levi’s",
	            "field": "category",
	            "id": "category:levi’s",
	            "priorW": 2,
	            "priorP": 0.5
	        }
	    },
	    {
	        "probability": 0.009240508102242771,
	        "variable": {
	            "feature": "guy's jeans",
	            "field": "category",
	            "id": "category:guy's jeans",
	            "priorW": 2,
	            "priorP": 0.5
	        }
	    },
	    {
	        "probability": 0.009240482454494279,
	        "variable": {
	            "feature": "levi’s 559™ relaxed straight",
	            "field": "category",
	            "id": "category:levi’s 559™ relaxed straight",
	            "priorW": 2,
	            "priorP": 0.5
	        }
	    },
	    {
	        "probability": 0.009240367820279082,
	        "variable": {
	            "feature": "jeans",
	            "field": "category",
	            "id": "category:jeans",
	            "priorW": 2,
	            "priorP": 0.5
	        }
	    },
	    {
	        "probability": 0.009237133622100569,
	        "variable": {
	            "feature": "shapewear & girdles",
	            "field": "category",
	            "id": "category:shapewear & girdles",
	            "priorW": 2,
	            "priorP": 0.5
	        }
	    },
	    {
	        "probability": 0.009235379278456897,
	        "variable": {
	            "feature": "levi’s 514™ straight",
	            "field": "category",
	            "id": "category:levi’s 514™ straight",
	            "priorW": 2,
	            "priorP": 0.5
	        }
	    },
		...

Given both the _name_title_ and _description_, aito can predict the _category_ with much higer accuracy


