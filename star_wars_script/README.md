## Star Wars Data

In this example, we will show how aito can predict the character based on a given dialouge. It will also show aito resiliency to small dataset

## The data
1. The star wars script dataset is taken from [Kaggle public dataset](https://www.kaggle.com/xvivancos/star-wars-movie-scripts)
2. The dataset was preprocessed and transformed to json format
3. The dataset contains 10148 rows for 1010 rows for the 1010 dialouges in Star Wars Episode IV. The columns include:

* index
* character
* dialouge

## Example usage

Let's use aito to predict who would say this line:
```
The Force will be with you
```

* __Using API__: __*Predict*__
* __Send query to https://aitoai-demo.api.aito.ninja/api/_predict__

[Query](queries/force.json)

	{
	   "context": "star_wars",
	    "q" : [
	    	{"when": "dialogue", "is": "The Force will be with you"}
	   ],
	   "predict":"character"
	}

[Result](results/force.json)


    {
        "probability": 0.23747173285901202,
        "variable": {
            "feature": "ben",
            "field": "character",
            "id": "character:ben",
            "priorW": 2,
            "priorP": 0.5
        }
    },
    {
        "probability": 0.1579250640540321,
        "variable": {
            "feature": "dodonna",
            "field": "character",
            "id": "character:dodonna",
            "priorW": 2,
            "priorP": 0.5
        }
    },
    {
        "probability": 0.10707165189904204,
        "variable": {
            "feature": "vader",
            "field": "character",
            "id": "character:vader",
            "priorW": 2,
            "priorP": 0.5
        }
    },
    ...

Obviously, this line belong to Ben.

Another line:

* __Using API__: __*Predict*__
* __Send query to https://aitoai-demo.api.aito.ninja/api/_predict__

[Query](queries/never_tell_me_the_odds.json)

	{
	   "context": "star_wars",
	    "q" : [
	    	{"when": "dialogue", "is": "Never tell me the odds"}
	   ],
	   "predict":"character"
	}

[Result](results/never_tell_me_the_odds.json)

	[
    {
        "probability": 0.16081875354109945,
        "variable": {
            "feature": "han",
            "field": "character",
            "id": "character:han",
            "priorW": 2,
            "priorP": 0.5
        }
    },
    ...

And aito predicts correctly that this line belongs to Han. Interestingly, this line was not given in the training dataset.




