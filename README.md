# aito.ai - DEMO #

## Preparation

1. Postman
2. URL: https://aitoai-demo.api.aito.ninja/
3. API key: k9y80rKmce9clHJ5EuTSY3uRNYZNHMO24JwbOoiB

## Structure
Each child directory will contain a demo of using aito.ai. Inside, you can find the data used in _data_ directory, the queries used for demo in _queries_ directory, and the expected results from executing the given queries in _results_. You can also find the instruction in README.md

1. [Analyze Products Data](analyze)
2. [Predict character based on line](star_wars_script)
3. [Project success analysis](kickstarter)


The directory [python](python) contains python script to test the performance of aito