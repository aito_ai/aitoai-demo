import json
import pandas as pd


def load_from_file_to_json(path):
	if path.endswith('.csv'):
		data = pd.read_csv(path)
		json_string = data.to_json(orient = "records")

	if path.endswith('json'):
		with open(path) as file:
			data = json.load(file)
		json_string = json.dumps(data)

	return json_string

def load_from_file_to_dataframe(path):
	if path.endswith('.csv'):
		dataframe = pd.read_csv(path)

	if path.endswith('json'):
		dataframe = pd.read_json(path)
	return dataframe

