import pandas as pd
import json

path = "../../kickstarter/data/kickstarter"

data = pd.read_csv(path + ".csv")
print(len(data))
with open(path + ".json", 'w') as outfile:
    parsed = json.loads(data)
    json.dump(parsed, outfile)
# print(data.columns.values)
# print(pd.io.json.build_table_schema(data, index = False)['fields'])