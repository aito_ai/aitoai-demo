"""
This script test the performance of aito predicting category of products
"""
import os
import sys
import pandas as pd
import numpy as np
import requests
import json
from pprint import pprint
sys.path.append(os.path.join(os.path.dirname(__file__),'../../'))
from python.common import *
import timeit



def naive_classifier(table, prediction_feature):
	"""Summary
	This function simply choose the feature appears the most
	
	Args:
	    table (dataframe): Description
	    prediction_feature (string): Description
	
	Returns:
	    int: accuracy
	"""

	unique_features = table[prediction_feature].unique()

	n_rows = len(table)
	accuracy = -1

	for feature in unique_features:
		feature_rows = table[table[prediction_feature] == feature]
		accuracy = max(accuracy, len(feature_rows)/n_rows)

	return accuracy


def test_predictor(table, table_name, conditional_features, prediction_feature, env_url, api_key, n_top_predictions):
	"""Summary
	Test the accuracy of aito predictor on a table
	Args:
	    table (dataframe): training data
	    table_name (str): name of the table to be used in "context" in aito query
	    conditional_features ([str]): list of name of conditional features to be used with "when" in aito query 
	    prediction_feature (str): name of feature to be predicted
	    env_url (str): Description
	    api_key (str): Description
	    n_top_predictions (int): number of alternative results to be taken into consideration instead of just top result
	
	Raises:
	    ValueError: Description
	"""
	headers = {}
	headers['Content-Type'] = "application/json; charset=utf-8"
	headers['x-api-key'] = api_key
	
	n_prediction_correct = np.zeros(len(table))
	n_prediction_correct_alternative = np.zeros((len(n_top_predictions), len(table)))

	request_results  = []

	for index, row in table.iterrows():
		print(index)
		conditions = {}
		conditions['when'] = {}

		for conditional_feature in conditional_features:
			conditions['when'][conditional_feature] = ("is", json.dumps(row[conditional_feature]))

		query = query_generator.generate_predict_query(table_name, conditions, prediction_feature, True)

		start_requesting = timeit.default_timer()
		request = requests.post(env_url+"/api/_predict", data = query, headers = headers)
		end_requesting = timeit.default_timer()

		if request.status_code != 200:
			raise ValueError('Something is wrong', request.reason)

		request_header = request.headers
		request_result = json.loads(request.text)

		predicted_result = request_result[0]['variable']['feature']
		actual_result = row[prediction_feature]

		if predicted_result == actual_result:
			if index != 0:
				n_prediction_correct[index] = n_prediction_correct[index - 1] + 1
			else: 
				n_prediction_correct[index] = 1
		else:
			if index != 0:
				n_prediction_correct[index] = n_prediction_correct[index - 1]

		# print('Actual result: ', actual_result)
		# for i in range(n_top_predictions[len(n_top_predictions) - 1]):
			# print('Alternative Result ', i, request_result[i]['variable']['feature'])

		for i in range(len(n_top_predictions)):
			prediction_correct = False
			for j in range(n_top_predictions[i]):
				alternative_result = request_result[j]['variable']['feature']
				if alternative_result == actual_result:
					prediction_correct = True

			if prediction_correct:
				if index != 0:
					n_prediction_correct_alternative[i, index] += n_prediction_correct_alternative[i, index - 1] + 1
				else:
					n_prediction_correct_alternative[i, index] = 1
			else:
				if index != 0:
					n_prediction_correct_alternative[i, index] += n_prediction_correct_alternative[i, index - 1]

		request_result = {}
		request_result['query'] = query
		# request_result['headers'] = request.headers
		request_result['actual_time'] = end_requesting - start_requesting 
		request_results.append(request_result)


	accuracy_naive_classifier = naive_classifier(table, "category")


	output_file_path = 'io/' + '_'.join(conditional_features) + "_to_" + prediction_feature + '/'
	sys.stdout = open(output_file_path + 'accuracy', 'w')

	accuracy = n_prediction_correct[len(table) - 1]/len(table)
	accuracy_alternative = np.zeros(len(n_top_predictions))
	for i in range(len(n_top_predictions)):
		accuracy_alternative[i] = n_prediction_correct_alternative[i, len(table) - 1]/len(table)

	print('Number of samples: ', len(table))	
	print('accuracy of naive classifier: ', accuracy_naive_classifier)
	print('accuracy of aito predict api: ', accuracy)
	for i in range(len(n_top_predictions)):
		print('accuracy when counting', n_top_predictions[i], 'n_top_predictions:', accuracy_alternative[i])

	n_prediction_correct_df = pd.DataFrame(n_prediction_correct)
	n_prediction_correct_df.to_csv(output_file_path + "n_prediction_correct.csv", index = False, index_label = False, header = False)
	n_prediction_correct_alternative_df = pd.DataFrame(n_prediction_correct_alternative)
	n_prediction_correct_alternative_df.to_csv(output_file_path + "n_prediction_correct_alternative.csv", index = False, index_label = False, header = False)

	
	sys.stdout = open(output_file_path + "query_results.txt", 'w')
	pprint(request_results)
	sys.stdout = sys.__stdout__

def main():
	"""Summary
	Take 2 arguments: env url, api key
	Raises:
	    ValueError: Description
	"""
	if len(sys.argv) < 3:
		raise ValueError('Missing arguments. Requires 2 arguments in the following order: env_url, api_key')

	env_url = sys.argv[1]
	api_key = sys.argv[2]

	# print(env_url)
	# print(api_key)
	# print("@@@@@@@@@@@@@@@@@@@@@@@@")

	error_handler.env_url_check(env_url)
	error_handler.api_key_check(api_key)

	products = content_loader.load_from_file_to_dataframe("../../analyze/data/products.csv")
	# products = products[:10]

	# test_predictor(products, "products", ["name_title"], "category", env_url, api_key, [1, 2, 3, 4, 6])
	# test_predictor(products, 'products', ["description"], "category", env_url, api_key, [1, 2, 3, 4, 5])
	test_predictor(products, 'products', ["name_title", 'description'], "category", env_url, api_key, [1, 2, 3, 4, 5])

main()