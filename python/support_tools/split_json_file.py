import json

file_path = "../generate_data/generated_data/json/"
file_name = "products"
n_split = 3

with open(file_path + file_name + ".json") as f:
	data = json.load(f)

# print(len(data))


for i in range(n_split):
	begin_index = 0 + i * (len(data) // n_split)
	if i != n_split - 1:
		end_index = begin_index + (len(data) // n_split)
	else:
		end_index = len(data)
	data_split = data[begin_index: end_index]
	with open(file_path + file_name + "_splited_" + str(i) + ".json", 'w') as outfile:
		json.dump(data_split, outfile)
