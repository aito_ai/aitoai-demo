import pandas as pd
import json

path = "../../kickstarter/data/ks-projects-201801.csv"

data = pd.read_csv(path)
print(len(data))
print(data.columns.values)
print(pd.io.json.build_table_schema(data, index = False)['fields'])