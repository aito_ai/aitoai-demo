import requests
import json
from pprint import pprint

def send_request(env_url, api_key, table_name, json_string):
	# Create request
	headers = {}
	headers['Content-Type'] = "application/json; charset=utf-8"
	headers['x-api-key'] = api_key

	url = env_url + "/api/data/" + table_name + "/batch"

	# print('url: ', url)
	# print('data: ', json_string)

	request = requests.post(url, data = json_string, headers = headers)
	if request.status_code != 200:
		raise ValueError('Something is wrong: ', request.text)

	request_result = json.loads(request.text)
	print('Load successful ')
	pprint(request_result)


