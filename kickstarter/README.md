## Analyze Products Data

Aito can analyze if a project will be succesful and the reason behind it. Let's try this by taking a look at the kickstarter projects data

## The data
1. The training dataset is a dataset that contains information about Kickstarter project
2. The training dataset is taken from [Kaggle public dataset](https://www.kaggle.com/kemical/kickstarter-projects/)
3. This is a subset (50000 projects) taken from the original dataset (more than 300000 projects) 
4. Each project has the following features:

* ID
* name
* category
* main_category
* currency
* deadline
* goal
* launched
* pledged
* state
* backers
* country
* usd_pledged
* usd_pledged_real
* usd_goal_real

## Example usage

### Predict if a project will be successful, failed, or cancelled

* __Using API__: __*Predict*__
* __Send query to https://aitoai-demo.api.aito.ninja/api/_predict__

[Query](queries/predict_espresso_bar.json)

	{
	   "context": "kickstarter",
	    "q" : [
	    	{"when":"name", "is":"Monarch Espresso Bar"},
	    	{"when":"category", "is":"Restaurants"},
	    	{"when":"main_category", "is": "food"},
	    	{"when":"country", "is": "US"}
	   ],
	   "predict":"state"
	}

[Result](results/predict_espresso_bar.json)

	[
	    {
	        "probability": 0.4606795717687758,
	        "variable": {
	            "feature": "fail",
	            "field": "state",
	            "id": "state:fail",
	            "priorW": 2,
	            "priorP": 0.5
	        }
	    },
	    {
	        "probability": 0.4438357115411697,
	        "variable": {
	            "feature": "success",
	            "field": "state",
	            "id": "state:success",
	            "priorW": 2,
	            "priorP": 0.5
	        }
	    },
	    {
	        "probability": 0.07962922891653372,
	        "variable": {
	            "feature": "cancel",
	            "field": "state",
	            "id": "state:cancel",
	            "priorW": 2,
	            "priorP": 0.5
	        }
	    },
	    ...

In this case, aito predicts incorrectly since aito predicts that the project will fail but in reality, the project is actually successful.
However, when taking a closer look at the project data: 

	{
		"ID": "1000014025", 
		"name": "Monarch Espresso Bar", 
		"category": "Restaurants", 
		"main_category": "Food", 
		"currency": "USD", 
		"deadline": "2016-04-01", 
		"goal": 50000.0, 
		"launched": "2016-02-26 13:38:27", 
		"pledged": 52375.0, 
		"state": "successful", 
		"backers": 224, 
		"country": "US", 
		"usd_pledged": 52375.0, 
		"usd_pledged_real": 52375.0, 
		"usd_goal_real": 50000.0
	}


We can see that this project barely made the pledge_goal (pledged 52375.0 and pledge_goal is 50000.0). Looking at aito's prediction, aito acutally predicts that the probability that the project will fail and the probability that the project will success is quite close (46% comparing to 44%)


Let's try another project

* __Using API__: __*Predict*__

[Query](queries/predict_teddy_bear.json)

	{
	   "context": "kickstarter",
	    "q" : [
	    	{"when":"name", "is":"Superhero Teddy Bear"},
	    	{"when":"category", "is":"DIY"},
	    	{"when":"main_category", "is": "Crafts"},
	    	{"when":"country", "is": "GB"}
	   ],
	   "predict":"state"
	}

[Result](results/predict_teddy_bear.json)

	[
	    {
	        "probability": 0.5546344610979445,
	        "variable": {
	            "feature": "fail",
	            "field": "state",
	            "id": "state:fail",
	            "priorW": 2,
	            "priorP": 0.5
	        }
	    },
	    {
	        "probability": 0.22590220256594154,
	        "variable": {
	            "feature": "success",
	            "field": "state",
	            "id": "state:success",
	            "priorW": 2,
	            "priorP": 0.5
	        }
	    },
	    {
	        "probability": 0.200877561344252,
	        "variable": {
	            "feature": "cancel",
	            "field": "state",
	            "id": "state:cancel",
	            "priorW": 2,
	            "priorP": 0.5
	        }
	    },
	    ...

In this case, aito predicts that the project will have much higher chance to fail than to success and to be cancelled. This is a correct prediction since the project actually got 0 pledge. 

	{
		"ID": "1000103948", 
		"name": "Superhero Teddy Bear", 
		"category": "DIY", 
		"main_category": "Crafts", 
		"currency": "GBP", 
		"deadline": "2016-01-05", 
		"goal": 12000.0, 
		"launched": "2015-12-06 20:09:06", 
		"pledged": 0.0, 
		"state": "failed", 
		"backers": 0, 
		"country": "GB", 
		"usd_pledged": 0.0, 
		"usd_pledged_real": 0.0, 
		"usd_goal_real": 17489.65
	}

### Explain the reason behind prediction

The better question is why would aito predict a project to be a failure.

* __Using API__: __*Relate*__
* __Send query to https://aitoai-demo.api.aito.ninja/api/_relate__

[Query](queries/relate_teddy_bear.json)

	{
	   "context": "kickstarter",
	    "q" : [
	    	{"when":"name", "is":"Superhero Teddy Bear"},
	    	{"when":"category", "is":"DIY"},
	    	{"when":"main_category", "is": "Crafts"},
	    	{"when":"country", "is": "GB"}
	   ],
	   "to":[{"field":"state", "is":"fail"}]
	}

[Result](results/relate_teddy_bear.json)

Aito would provides information that could be useful to redirect the project to be more sucessful. For example:

	"feature": "success",
    "field": "state",
        "conditional": [
            {
                "negationLift": 1.1630940651485937,
                "samples": 1174,
                "whenVariableStates": [
                    {
                        "index": 0,
                        "state": true,
                        "field": "main_category",
                        "feature": "craft"
                    }
                ],
                "frequency": 282,
                "lift": 0.706928187862687,
                "probability": 0.2527497072175598
            }
        ]

Aito.ai shows that when the _main_category_ is _craft_, it is 70% less likely that the project would be a success. 


	"feature": "cancel",
	"field": "state",
	"conditional": [
	    {
	        "negationLift": 0.9469089444757965,
	        "samples": 26,
	        "whenVariableStates": [
	            {
	                "index": 0,
	                "state": true,
	                "field": "name",
	                "feature": "superhero"
	            }
	        ],
	        "frequency": 5,
	        "lift": 1.4683163602157223,
	        "probability": 0.14950778039220006
	    }
	],

This result means that when the _name_ of the project has _superhero_ in it, it is 1.5 times more likely that the project would be cancelled.

	"feature": "cancel",
	"field": "state",
	"conditional": [
	    {
	        "negationLift": 0.9380774504383679,
	        "samples": 19,
	        "whenVariableStates": [
	            {
	                "index": 0,
	                "state": true,
	                "field": "name",
	                "feature": "teddi"
	            }
	        ],
	        "frequency": 4,
	        "lift": 1.5462189956415695,
	        "probability": 0.157440028799151
	    }
	],

Another interesting insights, when the project has _name_ contains _teddi_, it is also 1.5 times more likely that the project would be cancelled. In conclusion, aito not only is able to produce quality prediction for project sucess but also provides reveals the reason for the project failure to make decision upon it. 